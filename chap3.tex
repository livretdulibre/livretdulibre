\section{Pourquoi utiliser les Logiciels Libres ?}

\subsection{Éthique}

Tout d'abord, utiliser des Logiciels Libres est une démarche éthique :
c'est la volonté d'utiliser des logiciels réalisés avec l'objectif de
créer un bien commun dans l'intérêt général, et non pas des logiciels
créés pour servir des intérêts privés.

Par exemple, l'idée de partage des connaissances véhiculée par la
philosophie du Logiciel Libre est en adéquation avec les valeurs de
l'enseignement public~\cite{ll-utbm}. L'utilisation de tels logiciels
par l'éducation nationale serait donc cohérente~\cite{abuledu}.

\subsection{Libre accès}

Les Logiciels Libres sont librement accessibles, copiables et
diffusables. Leur utilisation permet donc un accès moins onéreux aux
technologies de l'information. Leur coût, généralement plus faible que
celui des logiciels propriétaires, permet d'engendrer des économies
sur l'achat des licences. Les économies ainsi réalisées peuvent par
exemple permettre d'investir dans des développements pour améliorer
les Logiciels Libres utilisés, dans de la formation ou du support
technique. Ces investissements peuvent ainsi financer des sociétés de
services locales plutôt que des éditeurs de logiciels situés la
plupart du temps à l'étranger.

Pour le responsable informatique d'une grande structure, la gestion
des licences est simplifiée avec le Logiciel Libre : on peut le copier
sans compter.

\subsection{Indépendance et pérennité}
\label{secformats}

La disponibilité du code source des Logiciels Libres permet d'être
indépendant du fournisseur de logiciel. Ceci a plusieurs avantages~:

\begin{itemize}

\item si le fournisseur de logiciels disparaît ou change son offre, il est
  possible de faire appel à un autre fournisseur, en conservant la
  solution technique actuelle~;

\item il est possible de modifier soi-même le code source du logiciel,
  ou d'employer des développeurs, afin d'ajouter des fonctionnalités
  ou de corriger des erreurs~;

\item l'utilisateur maîtrise totalement son environnement informatique
: il n'est limité que par ses propres connaissances.

\end{itemize}

D'autre part, les Logiciels Libres utilisent en général des formats et
des protocoles ouverts~\cite{openformats}, dont le fonctionnement est publiquement connu.
Leur usage permet à l'utilisateur de ne pas être dépendant d'un
logiciel particulier : il peut en changer librement.

\subsection{Qualités techniques}

La disponibilité du code source permet à des milliers de développeurs
de vérifier en permanence ce code source, améliorant ainsi la
fiabilité et la sécurité des Logiciels Libres. En particulier, la
communauté du Logiciel Libre est très réactive en ce qui concerne
la correction des problèmes de sécurité par rapport aux éditeurs de
logiciels propriétaires.

Les Logiciels Libres sont souvent indépendants des contraintes
commerciales~: le développement n'est pas dirigé par la rentabilité et
les corrections de problèmes mineurs ne sont donc pas négligées. Pour
beaucoup de ces logiciels, il n'y a pas d'impératifs de calendrier~:
les nouvelles versions sortent lorsqu'elles ont un atteint un niveau
de qualité suffisant.

\subsection{Communauté du Logiciel Libre}

Autour de l'idée du Libre et des Logiciels Libres s'est constituée une
importante communauté de programmeurs, d'utilisateurs, de traducteurs,
de graphistes. Ceux-ci travaillent dans des dizaines de milliers de
projets, dont l'envergure et le mode d'organisation sont très variables.
Cependant, dans la majorité des cas, le développement est massivement
décentralisé et Internet est au coeur de l'organisation des
projets. C'est grâce au courrier électronique, aux listes de
discussions et à tous les autres outils qu'offre Internet que le
Logiciel Libre peut prospérer. Il est intéressant de noter qu'Internet
existe aussi grâce au Logiciel Libre : de nombreux logiciels
d'infrastructure nécessaires à son fonctionnement sont des Logiciels
Libres.

Dans ce modèle communautaire de développement, la relation entre
l'utilisateur et le développeur n'est plus une relation de client à
fournisseur, mais de personne à personne, privilégiant l'entraide.
L'utilisateur devient acteur du processus de création, et participe à
l'amélioration du logiciel. Les Logiciels Libres disposent donc d'une
importante base d'utilisateurs-testeurs, avec laquelle les éditeurs de
logiciels propriétaires peuvent difficilement rivaliser.

Les Logiciels Libres sont réalisés par des milliers de développeurs à
travers le monde. Ces personnes travaillent pour des entreprises, des
administrations publiques ou territoriales, des laboratoires de
recherche publics ou privés, ou sont de simples programmeurs
passionnés, férus d'informatique, les {\em hackers}\footnote{Les {\em
hackers} ne sont pas des délinquants informatiques, mais des férus de
technologie et de programmation (voir \cite{comment-devenir-hacker} et
\cite{ethique-hacker}). Le terme a été détourné par les médias de
masse depuis le début des années 1990.}. Ces derniers sont le plus
souvent motivés par l'acquisition de connaissances nouvelles, le
plaisir de produire un objet technique satisfaisant ou le désir
d'acquérir une réputation dans le milieu des développeurs de Logiciels
Libres.
