\section{Se libérer au quotidien}

\subsection{Éviter l'utilisation de formats fermés}

Un format de fichier décrit la manière dont sont stockées les
informations.  En général, les Logiciels Libres utilisent des formats
et des protocoles ouverts, c'est-à-dire des formats de fichiers ou des
protocoles de communication dont les spécifications sont librement
accessibles~\cite{loiformats}. Même lorsque vous utilisez des
applications propriétaires, il est important d'utiliser des normes ou
des standards ouverts. Leur utilisation offre plusieurs avantages :

\begin{description}

 \item [Pérennité :] elle permet de garantir la pérennité des
 informations, qui est une question importante pour les
 administrations et les entreprises. En effet, que se passerait-il si
 dans 10 ans on ne pouvait plus lire des documents importants parce
 que la société qui éditait le logiciel a fait faillite ou que le
 format de fichier est devenu obsolète ?

 \item [Interopérabilité :] elle permet également de garantir
 l'interopérabilité, c'est-à-dire la capacité à travailler avec des
 systèmes et des logiciels différents, au choix de l'utilisateur. En
 utilisant un standard ouvert, vous pouvez envoyer un document à un
 correspondant sans le contraindre à utiliser un logiciel particulier
 pour le lire.

 \item [Indépendance technique :] les solutions techniques étant
 interopérables, l'utilisateur n'est pas enfermé dans l'utilisation
 d'un logiciel en particulier, et peut en changer librement.

\end{description}

Parmi les formats fermés à éviter absolument, on peut par exemple
citer le format de fichiers {\em doc} de Microsoft Word. En plus de
tous les problèmes liés à l'interopérabilité et à la pérennité des
données (le format {\em doc} changeant régulièrement), il est un
vecteur de propagation de virus informatiques, et il contient
l'historique des modifications du document, permettant à votre
correspondant de lire dans vos pensées... Il est préférable d'utiliser
les formats odt (OpenDocument) ou texte brut (pour les documents texte
modifiables) ou le format PDF (pour tous les documents non
modifiables). De la même façon, il existe des formats ouverts pour les
images, la musique ou la vidéo~\footnote{tels que jpeg, ogg et mkv}. Des
informations plus précises peuvent être trouvées sur le
Web~\cite{openformats}.

\subsection{Éviter la copie illégale de logiciels}

À l'heure actuelle, le taux de copie illégale de logiciels est
relativement élevé, notamment chez le particulier. On pourrait penser
que les éditeurs sont complètement démunis face à cet état de fait.

En fait, ces entreprises sont tout à fait conscientes qu'il
est souvent impossible pour un particulier d'acheter tous les
logiciels qu'il utilise. La copie illégale réalisée par les
particuliers est même pour ces entreprises un puissant levier
marketing car elle permet de familiariser les particuliers avec
l'usage de ses logiciels. Par la suite, les décideurs
d'entreprise préféreront installer des logiciels que leurs
employés connaissent déjà, même si des produits concurrents
moins onéreux ou plus adaptés pouvaient être préférés.

La copie illégale peut ainsi se révéler favorable à l'entreprise qui
la subit, au détriment de ses concurrents et de l'utilisation de
Logiciels Libres équivalents.  Parfois, la copie illégale est si
répandue chez les particuliers qu'ils ignorent jusqu'à l'existence de
logiciels équivalents, comme les Logiciels Libres, qu'ils pourraient
utiliser en toute légalité. Un article~\cite{copieillegale} donne de
plus amples informations concernant ce problème.

\subsection{Privilégier l'utilisation de Logiciels Libres}

\subsubsection{Utiliser GNU/Linux}

Remplacer son système d'exploitation propriétaire par GNU/Linux est
accessible à n'importe quel amateur motivé. GNU/Linux est disponible
sous la forme de distributions. Une distribution est une compilation
de nombreux logiciels, accompagnée d'un système d'installation simple
et d'un système de paquets permettant l'installation, la suppression
et la mise à jour des différents logiciels installés sur le
système. Une distribution contient des logiciels adaptés à tous les
usages : système d'exploitation, bureautique, Internet, multimédia,
développement, serveur{\dots} Il n'est en général pas nécessaire
d'ajouter d'autres logiciels.

Les distributions LinuxMint, Ubuntu ou Mageia par exemple sont
disponibles librement sur Internet. On trouve de nombreuses
introductions et documentations sur le Web. Des groupes d'utilisateurs
de Logiciels Libres~\cite{afulgul} peuvent également guider les
nouveaux utilisateurs dans la découverte et l'utilisation de ces
logiciels.

Passer sous GNU/Linux apporte de nombreux d'avantages : il n'existe
quasiment aucun virus pour les systèmes libres tels que GNU/Linux, si
bien qu'un logiciel anti-virus est inutile. De plus, les {\em
spywares} ou {\em espiogiciels} \footnote{logiciels espions qui
récoltent des données à des fins publicitaires ou permettent
d'utiliser à distance votre ordinateur à des fins malveillantes} et
autres {\em adwares}\footnote{logiciels affichant des publicités non
sollicitées à l'écran} sont également absents de tous les Logiciels
Libres. Mais utiliser GNU/Linux pose aussi quelques problèmes :
certains logiciels, notamment des jeux, des logiciels culturels et des
logiciels pour professionnels, n'ont pas d'équivalent sous
GNU/Linux. Certains périphériques ne fonctionnent pas non plus
parfaitement avec GNU/Linux car les constructeurs négligent pour
l'instant cette plateforme.

\subsubsection{Utiliser des Logiciels Libres avec un système propriétaire}

Par ailleurs, il est tout à fait possible d'utiliser des Logiciels
Libres en conservant votre système d'exploitation propriétaire comme
Microsoft Windows ou Mac OS X. De nombreux Logiciels Libres y sont
disponibles, comme le navigateur Web Mozilla Firefox, le lecteur de
courrier électronique Mozilla Thunderbird~\cite{mozilla}, la suite
bureautique LibreOffice ou le logiciel de retouche
photo et de dessin The Gimp. Des compilations telles que
Framakey~\footnote{Voir le site {\em
    http://www.framakey.org/}} contiennent un grand nombre de
Logiciels Libres pour Windows.

Une autre solution pour tester aisément les Logiciels Libres est
d'installer une distribution sur un CD ou une clef USB (en mode {\em
  LiveCD}). Cela permet de lancer un système GNU/Linux complet sans
rien installer sur son ordinateur: tous les programmes fonctionnent
directement à partir de la clef USB. Et si vous aimez ce que vous
voyez, vous pouvez alors cliquer sur «installer». Vous aurez le choix
entre installer linux à côté du système existant, ce qui n'effacera
aucune donnée, ou bien effacer le système existant et installer linux
sur tout le disque dur. Avec la première option, vous aurez à choisir
entre windows ou linux à chaque démarrage de l'ordinateur.

\subsection{Participer au mouvement du Libre}

Il existe de multiples manières de participer au mouvement du Libre :

\begin{itemize}

\item Vous pouvez utiliser des logiciels libres, même sur un système
  propriétaire~\footnote{un bon point de départ est le site {\em http://framastart.org/}}~;

\item Vous pouvez en parler autour de vous~;

\item Vous pouvez participer au financement de projets ou soutenir des
associations~;

\item Si vous êtes artiste, écrivain, musicien, poète, compositeur,
peintre, philosophe, ou tout simplement si vous produisez des
{\oe}uvres, vous pouvez placer une partie de celles-ci sous une
licence libre, afin de participer à la constitution d'un fonds commun
de ressources libres~;

\item Si vous êtes informaticien, vous pouvez participer au
développement de Logiciels Libres~;

\item Si vous êtes doué en français ou dans une autre langue, vous
pouvez participer à la traduction de logiciels, documentations et
documents. Cela nécessite très peu de connaissances techniques~;

\item Si vous êtes graphiste, artiste ou musicien, vous pouvez aider à
améliorer l'interface des logiciels, ou participer au développement de
jeux libres~;

\item Vous pouvez tester les Logiciels Libres en cours de
développement et ainsi aider leurs auteurs à les améliorer~;

\item Vous pouvez inciter les responsables informatiques de votre
  entreprise ou de votre université à utiliser des Logiciels Libres et
  des formats ouverts~;

\item Vous pouvez contribuer à l'encyclopédie libre
Wikipédia~\cite{wikipedia};

\item Vous pouvez distribuer ce livret autour de vous, aider à
l'améliorer, et informer votre entourage de l'alternative que représentent
les Logiciels Libres par rapport aux logiciels propriétaires.

\end{itemize}
