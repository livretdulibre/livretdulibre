doc_tex = livret_libre.tex
### Documents

all: $(doc_tex:.tex=)_a4.ps

$(doc_tex:.tex=)_a4_affiche.dvi: $(wildcard *.tex) $(wildcard *.bib)
	rm -f margins.tex
	ln -s margins_a4_affiche.tex margins.tex
	./builddvi.sh $(doc_tex:.tex=) $(doc_tex:.tex=_a4_affiche.dvi)


$(doc_tex:.tex=)_a4.dvi: $(wildcard *.tex) $(wildcard *.bib)
	rm -f margins.tex
	ln -s margins_a4.tex margins.tex
	./builddvi.sh $(doc_tex:.tex=) $(doc_tex:.tex=_a4.dvi)

$(doc_tex:.tex=)_a5_10pt.dvi: $(wildcard *.tex) $(wildcard *.bib)
	rm -f margins.tex
	ln -s margins_a5_10pt.tex margins.tex
	./builddvi.sh $(doc_tex:.tex=) $(doc_tex:.tex=_a5_10pt.dvi)

$(doc_tex:.tex=)_a5_11pt.dvi: $(wildcard *.tex) $(wildcard *.bib)
	rm -f margins.tex
	ln -s margins_a5_11pt.tex margins.tex
	./builddvi.sh $(doc_tex:.tex=) $(doc_tex:.tex=_a5_11pt.dvi)

$(doc_tex:.tex=_a5duplex_10pt.ps): $(doc_tex:.tex=_a5_10pt.ps)
	psbook $(doc_tex:.tex=_a5_10pt.ps) $(doc_tex:.tex=_tmp.ps)
	pstops \
	"4:0L(29.7cm,0cm)+1L(29.7cm,14.85cm),2R(-8.7cm,29.7cm)+3R(-8.7cm,14.85cm)"\
	$(doc_tex:.tex=_tmp.ps) $(doc_tex:.tex=_a5duplex_10pt.ps)
	rm $(doc_tex:.tex=_tmp.ps)

$(doc_tex:.tex=_a5duplex_11pt.ps): $(doc_tex:.tex=_a5_11pt.ps)
	psbook $(doc_tex:.tex=_a5_11pt.ps) $(doc_tex:.tex=_tmp.ps)
	pstops \
	"4:0L(29.7cm,0cm)+1L(29.7cm,14.85cm),2R(-8.7cm,29.7cm)+3R(-8.7cm,14.85cm)"\
	$(doc_tex:.tex=_tmp.ps) $(doc_tex:.tex=_a5duplex_11pt.ps)
	rm $(doc_tex:.tex=_tmp.ps)

%.ps: %.dvi
	dvips -t a4 -G0 $< -o $@

$(doc_tex:.tex=)_a4_lecture.pdf: $(wildcard *.tex) $(wildcard *.bib)
	# nettoyage nécessaire.
	rm -f $(doc_tex:.tex=).toc
	rm -f $(doc_tex:.tex=).aux
	rm -f margins.tex
	ln -s margins_a4pdf_lecture.tex margins.tex
	./buildpdf.sh $(doc_tex:.tex=) $(doc_tex:.tex=_a4_lecture.pdf)


# d'après http://linuxfr.org/comments/306028,1.html
%.pdf: %.ps
	ps2pdf -dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true -dCompatibilityLevel=1.3 $< $@

# A utiliser pour générer tous les fichiers lors de la sortie d'une nouvelle
# édition.
release=-ed3
targetsps=$(doc_tex:.tex=_a4.ps) $(doc_tex:.tex=_a4_affiche.ps) $(doc_tex:.tex=_a5duplex_10pt.ps) $(doc_tex:.tex=_a5duplex_11pt.ps)
targetspdf=$(targetsps:.ps=.pdf)
release: $(targetsps) $(targetspdf) $(doc_tex:.tex=_a4_lecture.pdf)
	mv $(doc_tex:.tex=_a4_lecture.pdf) $(doc_tex:.tex=_a4_lecture$(release).pdf)
	for f in ps pdf ; do \
		for v in a5duplex_10pt a5duplex_11pt a4 a4_affiche ; do \
			mv $(doc_tex:.tex=_$$v.$$f) $(doc_tex:.tex=_$$v$(release).$$f) ; \
		done; \
	done

html:
	htlatex ${doc_tex} xhtml,fn-in,png " -g png"
	bash fixhtml.bash

txt: $(doc_tex:.tex=_a4.ps)
	ps2ascii $(doc_tex:.tex=_a4.ps) $(doc_tex:.tex=.txt)
	sed -i "s/'e/é/g" $(doc_tex:.tex=.txt)
	sed -i 's/`e/è/g' $(doc_tex:.tex=.txt)
	sed -i 's/`a/à/g' $(doc_tex:.tex=.txt)
	sed -i "s/'E/É/g" $(doc_tex:.tex=.txt)
	sed -i 's/`A/À/g' $(doc_tex:.tex=.txt)


clean:
	rm -f *.ps livret_libre*.pdf *.dvi $(doc_tex:.tex=.toc) $(doc_tex:.tex=.aux) $(doc_tex:.tex=.log) $(doc_tex:.tex=.blg) $(doc_tex:.tex=.out) $(doc_tex:.tex=.bbl) $(doc_tex:.tex=.fot) $(doc_tex:.tex=.txt) *~

urlcheck:
	for i in $$(grep url{http livret_libre.bib | sed 's/.*{\(.*\)}.*/\1/'); do \
		echo -n "$$i : "; \
		if [ $$(wget -O /dev/null -nv $$i 2>&1| wc -l) -eq 1 ]; then echo OK; else \
			echo PROBLEM ; \
		fi ; \
	done
