Livret du Libre

Ma version de http://www.livretdulibre.org/

Le Livret du Libre est une introduction au Libre accessible aux
non-informaticiens. Ce document d'une vingtaine de pages aborde
notamment :

*  Les origines et les bases éthiques et morales du Libre
*  Le Logiciel Libre et le copyleft
*  Les raisons pour lesquelles il faut préférer le Logiciel Libre
*  D'autres initiatives visant à diffuser la connaissance (art Libre, documentation Libre, etc ...)
*  Les menaces sur la libre circulation de la connaissance (DMCA, brevets logiciels, ...) [partie supprimée par ma version]
*  Différents modèles économiques basés sur le Logiciel Libre
*  Diverses manières de participer au mouvement du Libre.

L'objectif n'est pas de faire un livre de référence permettant de tout
connaître sur tous les sujets, mais de faire un petit livret court,
accessible, que l'on peut distribuer et faire lire à n'importe
qui. Pour chaque thème abordé, de nombreuses références sont données
pour ceux qui souhaitent en savoir plus.

Changements apportés par cette version :
======================================

* j'ai mis à jour les vieilles références (OpenOffice devient
  LibreOffice, OpenCD => Framakey, la liste des distributions
  populaires, etc)
* j'ai rajouté quelques points innocents (comment participer, etc)
* j'ai supprimé la liste des menaces car, bien qu'important, c'est une
  partie vite datée,
* j'ai supprimé un passage avec lesquel je ne suis pas
  d'accord et que je trouve trop ingénu, c'est à dire la phrase d'introduction: _«Dans un certain
  idéal, la communauté scientifique n'aurait pour objectif que
  l'avancée de son domaine, sans avoir à tenir compte d'une
  application directe, en particulier mercantile.»_ Voir mes remarques
  sur [ce commit][commit].
* j'ai changé le gestionnaire de versions de svn à git.

On peut consulter tous les changements dans la [liste des «commits»][].

[commit]:https://framagit.org/livretdulibre/livretdulibre/commit/f237e8a09248e160b28acfa9a9e7ab8d388df469

[liste des «commits»]: https://framagit.org/livretdulibre/livretdulibre/commits/master



Paquets nécessaires à la génération du document :
==================================================

Ce livret est écrit en LaTeX.

Pour générer les versions PostScript et PDF de ce document, vous avez besoin
de plusieurs paquets qui ne sont pas forcément installés par défaut dans votre
distribution.

Sous Debian, il faut entre autres :

    texlive-latex-extra
    tetex-base tetex-bin tetex-extra gs psutils

et des paquets latex:

    texlive-lang-french texlive-lang-cyrillic

Sous Mandriva, il faut entre autres :

    tetex tetex-latex ghostscript psutils

Sous Gentoo, il faut entre autres :

    tetex ghostscript psutils

Génération d'un document PostScript ou PDF :
=====================================

Les commandes à effectuer sont les suivantes:

    latex livret_libre  # génère des erreurs "citation foo undefined"
    bibtex livret_libre # construit la bibliography
    latex livret_libre
    latex livret_libre  # et nous avons un fichier dvi.

Nous avons un fichiel .dvi, qu'il faut alors transformer en pdf:

    dvipdf livret_libre.dvi

Ce qui nous génère `livret_libre.pdf`.

Pour automatiser le processus, nous avons le Makefile.

Plusieurs documents peuvent être générés:

* une version A4 : taper make livret\_libre\_a4.ps ou simplement (c'est la cible par défaut du Makefile):

    make

* une version A5 pour impression sur imprimante supportant le duplex
  (recto-verso), en taille 10pt. Taper make livret\_libre\_a5duplex\_10pt.ps
* une version A5 / duplex en taille 11pt :
  Taper make livret\_libre\_a5duplex_11pt.ps
Pour générer des documents PDF, utilisez l'extension pdf ou lieu de ps.

Voici la liste exhaustive des cibles disponibles :

* livret\_libre\_a4.dvi
* livret\_libre\_a5\_10pt.dvi
* livret\_libre\_a5\_11pt.dvi
* livret\_libre\_a4.ps
* livret\_libre\_a5\_10pt.ps
* livret\_libre\_a5\_11pt.ps
* livret\_libre\_a5duplex\_10pt.ps
* livret\_libre\_a5duplex\_11pt.ps
* livret\_libre\_a4.pdf
* livret\_libre\_a5\_10pt.pdf
* livret\_libre\_a5\_11pt.pdf
* livret\_libre\_a5duplex\_10pt.pdf
* livret\_libre\_a5duplex\_11pt.pdf

Si vous avez besoin d'un autre mode d'impression, contactez-nous !

Attention :
===========

Il semble que certaines imprimantes non-PostScript aient des problèmes pour
imprimer les versions PostScript. Si vous rencontrez des problèmes, essayez
la version PDF.
