#!/bin/bash

sed -i 's/class="aebx-12x-x-144">\([0-9]\)<\/span>/class="aebx-12x-x-144">\1\&nbsp;\&nbsp;<\/span>/' livret_libre.html
sed -i 's/class="aebx-12x-x-120">\([0-9]\.[0-9]\)<\/span>/class="aebx-12x-x-120">\1\&nbsp;\&nbsp;<\/span>/' livret_libre.html
sed -i 's/class="aebx-12">\([0-9]\.[0-9]\.[0-9]\)<\/span>/class="aebx-12">\1\&nbsp;\&nbsp;<\/span>/' livret_libre.html
sed -i 's/\. \. \. En informatique/... En informatique/' livret_libre.html
sed -i 's/\(\[[0-9]*\]\)\(<a\)/<br\/>\1 \2/' livret_libre.html
sed -i 's/<br\/>\[1\]/<div style="text-align: left;">\[1\]/' livret_libre.html
sed -i 's/______*//' livret_libre.html
cat > livret_libre.css << EOF
body { font-family: "Bitstream Vera Sans", Verdana, Helvetica, Arial, sans-serif; margin: 20px 50px 20px 50px;}
p.indent { text-align: justify; }
.aebx-12x-x-120{font-weight: bold;}
.aebx-12x-x-144{font-size:172%;font-weight: bold;}
.aecsc-10x-x-358{font-size:358%;}
.aer-17{font-size:170%;}
.aebx-12x-x-172{font-size:206%;font-weight: bold;}
.cmmi-12x-x-120{font-style: italic;}
.aesl-12x-x-120{font-style: oblique;}
.aeti-12x-x-120{font-style: italic;}
.aebxti-10x-x-172{font-size:172%;font-weight: bold; font-style: italic;}
.small-caps{font-variant: small-caps; }
.htf-cmbx {font-weight: bold; font-style:normal;}
p.noindent { text-indent: 0em }
p.nopar { text-indent: 0em; }
p.indent{ text-indent: 1.5em }
a img { border-top: 0; border-left: 0; border-right: 0; }
center { margin-top:1em; margin-bottom:1em; }
td center { margin-top:0em; margin-bottom:0em; }
.Canvas { position:relative; }
img.math{vertical-align:middle;}
li p.indent { text-indent: 0em }
div.footnotes{border-top:solid 1px black; border-bottom:solid 1px black; padding-bottom:1ex; padding-top:0.5ex; margin-right:15%; margin-top:2ex; font-style:italic; font-size:80%;}
div.footnotes p{margin-top:0; margin-bottom:0; text-indent:0;}
span.footnotetext{ font-size:80%; font-style:italic; } 
.obeylines-h,.obeylines-v {white-space: nowrap; }
.overline{ text-decoration:overline; }
.overline img{ border-top: 1px solid black; }
td.displaylines {text-align:center; white-space:nowrap;}
.centerline {text-align:center;}
.rightline {text-align:right;}
div.verbatim {font-family: monospace; white-space: nowrap; }
span.fbox {padding-left:3.0pt; padding-right:3.0pt; text-indent:0pt; border:solid black 0.4pt; }
div.center, div.center div.center {text-align: center; margin-left:1em; margin-right:1em; margin-top:0.5em; margin-bottom:0.5em;}
div.center div {text-align: left;}
div.flushright, div.flushright div.flushright {text-align: right;}
div.flushright div {text-align: left;}
div.flushleft {text-align: left;}
.underline{ text-decoration:underline; }
.underline img{ border-bottom: 1px solid black; margin-bottom:1pt; }
.framebox-c, .framebox-l, .framebox-r { padding-left:3.0pt; padding-right:3.0pt; text-indent:0pt; border:solid black 0.4pt; }
.framebox-c {text-align:center;}
.framebox-l {text-align:left;}
.framebox-r {text-align:right;}
div.array {text-align:center;}
div.tabular, div.center div.tabular {text-align: center; margin-top:0.5em; margin-bottom:0.5em;}
table.tabular td p{margin-top:0em;}
div.td00{ margin-left:0pt; margin-right:0pt; }
div.td01{ margin-left:0pt; margin-right:5pt; }
div.td10{ margin-left:5pt; margin-right:0pt; }
div.td11{ margin-left:5pt; margin-right:5pt; }
td.td00{ padding-left:0pt; padding-right:0pt; }
td.td01{ padding-left:0pt; padding-right:5pt; }
td.td10{ padding-left:5pt; padding-right:0pt; }
td.td11{ padding-left:5pt; padding-right:5pt; }
.hline hr, .cline hr{ height : 1px; }
.tabbing-right {text-align:right;}
div.newtheorem { margin-bottom: 2em; margin-top: 2em;}
span.TEX {letter-spacing: -0.125em; }
span.TEX span.E{ position:relative;top:0.5ex;left:-0.0417em;}
a span.TEX span.E {text-decoration: none; }
span.LATEX span.A{ position:relative; top:-0.5ex; left:-0.4em; font-size:85%;}
span.LATEX span.TEX{ position:relative; left: -0.4em; }
.marginpar {width:20%; float:right; text-align:left; margin-left:auto; margin-top:0.5em; font-size:85%; text-decoration:underline;}
.marginpar p{margin-top:0.4em; margin-bottom:0.4em;}
div.float {text-align:center;}
div.figure {text-align:center;}
.equation td{text-align:center; }
td.equation { margin-top:1em; margin-bottom:1em; } 
td.eqnarray4 { width:5%; white-space: normal; }
td.eqnarray2 { width:5%; }
table.eqnarray-star, table.eqnarray {width:100%;}
div.eqnarray{text-align:center;}
div.pmatrix {text-align:center;}
span.pmatrix img{vertical-align:middle;}
div.pmatrix {text-align:center;}
img.cdots{vertical-align:middle;}
.frenchb-nbsp{font-size:75%;}
.frenchb-thinspace{font-size:75%;}
.equation td{text-align:center; }
table.align, table.alignat, table.xalignat, table.xxalignat, table.flalign, table.align-star, table.alignat-star, table.xalignat-star, table.flalign-star {width:100%; white-space: nowrap;}
td.align-label { width:5%; }
td.align-odd { text-align:right; padding-right:0.3em;}
td.align-even { text-align:left; padding-right:0.6em;}
td.gather-star, td.gather1 {text-align:center; }
.figure img.graphics {margin-left:10%;}
.ovalbox{border:solid thin;}
.Ovalbox-thick{border:solid thick;}
.doublebox{border:double thick;}
.shadowbox{border:solid thin; border-right:solid thick; border-bottom:solid thick;}

EOF
