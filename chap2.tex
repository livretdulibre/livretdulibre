\section{Le Logiciel Libre}

\subsection{Qu'est-ce que le Logiciel Libre ?}

Les licences\footnote{Une licence est un contrat décrivant les
conditions d'utilisation d'un logiciel par ses utilisateurs} de la
plupart des logiciels propriétaires sont prévues pour vous priver de
la liberté de les échanger ou de les modifier. À l'inverse, les
licences de Logiciel Libre~\cite{stallman-freesw} sont conçues pour
garantir quatres libertés :

\begin{itemize}

\item la liberté d'utiliser le logiciel pour n'importe quel usage et par
tout le monde~;

\item la liberté d'étudier le logiciel, et de l'adapter à ses
  besoins~;

\item la liberté de redistribuer des copies du logiciel comme de son
code source~;

\item la liberté d'améliorer le programme et de publier ses
modifications, pour en faire profiter toute la communauté.

\end{itemize}

Pour satisfaire la deuxième et la quatrième liberté, l'accès au code
source du logiciel est indispensable.

Richard Stallman fait régulièrement une analogie entre ces quatre
libertés et la devise de la République française : l'utilisateur est
libre d'utiliser son logiciel de la manière qui lui plaît. Les
utilisateurs sont égaux car ils peuvent tous utiliser les mêmes
logiciels. L'esprit de fraternité est respecté car l'utilisateur peut
aider ses proches.

\subsection{L'idée de \em{copyleft}}

Le {\em copyleft}  est une utilisation particulière du droit d'auteur
partant du principe que le partage doit fonctionner dans les deux
sens. Il  autorise la copie, la modification et la diffusion d'une
\oe{}uvre, en imposant que les versions modifiées faisant l'objet d'une
diffusion soient également disponibles sous une licence copyleft.
Ainsi, avec le copyleft, ce qui est libre reste libre pour toujours.

Le {\em copyleft} est un concept inventé par la Free Software
Foundation. Certaines personnes l'utilisent dans une démarche
militante en faveur du libre accès aux connaissances. Ce concept ne se
limite d'ailleurs plus qu'au logiciel.

Tout en respectant le droit de l'auteur, l'idée de {\em copyleft}
consiste à garantir les libertés du public en s'assurant que les
{\oe}uvres sous copyleft resteront libres. Il permet de créer un fonds
commun d'\oe{}uvres libres, considérant que la connaissance scientifique
et artistique fait partie du patrimoine de l'humanité.

La FAQ\footnote{{\em Frequently Asked Questions}, liste de questions
fréquemment posées} de la Licence Art Libre~\cite{lal-faq}, donnera de
plus amples informations sur la différence entre {\em copyright}, {\em copyleft},
et droits d'auteur.

\subsection{Les différences entre \emph{Logiciel Libre}, \emph{domaine public}, \emph{Freeware}, et \emph{Open Source}}

On confond souvent à tort le Logiciel Libre et d'autres types de
logiciels.

Une \oe{}uvre ou un logiciel dans le domaine public est libre de
droits, on peut en faire ce que l'on souhaite. Les Logiciels Libres ne
sont pas dans le domaine public : ils sont soumis au régime du droit
d'auteur et une licence précise les conditions de leur utilisation, de
leur modification et de leur distribution.

Les {\em Freewares} ou {\em graticiels} sont des logiciels
propriétaires gratuits, ils n'assurent donc pas les libertés associées
au Logiciel Libre. Cette confusion vient du fait qu'en anglais, Logiciel
Libre se dit \emph{Free Software}. \emph{Free} peut signifier \emph{libre}
ou \emph{gratuit}. Mais dans le cas du
Logiciel Libre, il faut comprendre {\em free} dans le sens {\em libre}
: un Logiciel Libre n'est pas forcément gratuit, même s'il l'est la
plupart du temps. En effet, une société peut commercialiser un
Logiciel Libre et réaliser des bénéfices sur cette
vente~\cite{stallman-selling}.

Afin de gommer cette confusion entre
{\em libre} et {\em gratuit}, certains acteurs du Logiciel Libre ont
introduit le terme {\em Open Source}~\cite{opensource} qui distingue
l'accès au code source de la gratuité.  Ce terme n'a finalement fait
qu'ajouter à la confusion. En effet, il a été exploité par des
sociétés diffusant leur code source mais sans toutes les libertés du
Logiciel Libre, et tout particulièrement celle de modifier le code
source et d'en diffuser les modifications.

Certains utiliseront le terme \emph{Open Source} en parlant de
Logiciels Libres sans faire l'amalgame avec les logiciels
gratuits. D'autres chercheront simplement à profiter de la popularité
grandissante des Logiciels Libres\footnote{Voir notamment Microsoft
\emph{Shared Source}~\cite{sharedsource}}. Il n'est pas toujours
simple de faire la différence.

La meilleure façon de savoir dans quel domaine se place le logiciel
que l'on utilise est de se référer à la licence d'utilisation sous
laquelle est distribué le logiciel. Le site Web de la Free Software
Foundation donne de plus amples explications sur les licences
(\cite{fsf-licenses} et \cite{gnu-licenses}).

%% J'ai mis ce paragrahe la, mais on avisera.
\subsection{GNU/Linux : un système abouti}

Le projet GNU a permis de créer un ensemble de logiciels, permettant
d'utiliser un ordinateur pour de nombreuses applications.  Toutefois,
jusqu'en 1991, un composant essentiel manquait : le noyau du système
d'exploitation. Véritable chef d'orchestre de l'ordinateur, il permet
aux différents logiciels de fonctionner en harmonie sur le système, et
leur permet d'utiliser les périphériques tels que l'écran, le clavier,
le réseau, la mémoire, etc.  C'est un projet nommé Linux, lancé par étudiant finlandais, Linus Torvalds, qui a comblé ce manque. Le système GNU/Linux était né. Il est constitué de divers
logiciels du projet GNU, du noyau Linux et de bien d'autres Logiciels
Libres.

À l'heure actuelle, le système GNU/Linux peut être utilisé
quotidiennement à la fois pour des usages courants tels que la
bureautique, l'Internet, le multimédia, mais aussi pour des usages
plus professionnels comme les serveurs Internet et le développement
logiciel. C'est d'ailleurs dans le domaine professionnel que GNU/Linux
a acquis ses lettres de noblesse. Le Logiciel Libre est donc une
alternative techniquement viable et crédible pour l'utilisateur.

Par exemple, le système GNU/Linux et d'autres Logiciels Libres sont
utilisés dans de nombreux routeurs ADSL/Wifi, dans deux tiers des
serveurs Web, dans la majorité des serveurs traitant le courrier
électronique, et dans la plupart des systèmes de calcul à haute
performance.
