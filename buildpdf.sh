#!/bin/sh

max_passes=7
p=0
if [ ! -f $1.pdf ]; then
	touch $1.pdf
fi
cp -f $1.pdf $1.pdf.tmp 
while [ $p -lt $max_passes ]
do 
	pdflatex $1.tex
	if [ $? -ne 0 ]; then
		echo "Latex returned with an error code : $?"
		exit 1
	fi
	if [ $p -eq 0 ] ; then
		bibtex $1
	fi
	p=`expr $p + 1`
	if cmp $1.pdf $1.pdf.tmp; then break; fi 
	cp -f $1.pdf $1.pdf.tmp
done
rm -f $1.pdf.tmp
# rename pdf file to $2
mv $1.pdf $2

if [ $p -ge $max_passes ] ; then
	echo "Max number of passes reached. PDF file couldn't be generated." 
	exit 1
else
	echo "Latex passes needed : $p"
fi 
