#!/bin/sh

max_passes=7
p=0
if [ ! -f $1.dvi ]; then
	touch $1.dvi
fi
cp -f $1.dvi $1.dvi.tmp 
while [ $p -lt $max_passes ]
do 
	latex $1.tex
	if [ $? -ne 0 ]; then
		echo "Latex returned with an error code : $?"
		exit 1
	fi
	if [ $p -eq 0 ] ; then
		bibtex $1
	fi
	p=`expr $p + 1`
	if cmp $1.dvi $1.dvi.tmp; then break; fi 
	cp -f $1.dvi $1.dvi.tmp
done
rm -f $1.dvi.tmp
# rename dvi file to $2
mv $1.dvi $2
if [ $p -ge $max_passes ] ; then
	echo "Max number of passes reached. DVI file couldn't be generated." 
	exit 1
else
	echo "Latex passes needed : $p"
fi 
