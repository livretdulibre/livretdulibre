\section{Le Libre : un retour naturel au partage du savoir}

\subsection{Partage des connaissances\dots}

\begin{flushright}
\textsl{Rien n'appartient à rien, tout appartient à tous.}

Alfred de Musset
\end{flushright}

Tout le monde connaît la formule $ a^2 + b^2 = c^2 $, connue sous le
nom de théorème de Pythagore.  Tous les jours, nous utilisons des
objets qui existent parce que ce théorème, comme beaucoup d'autres
théorèmes mathématiques et de découvertes scientifiques, est
disponible et utilisable librement. Cet accès libre et ce partage du
savoir sont à la base du développement et du progrès de l'humanité.

Ce partage naturel du savoir se situe dans la tradition du travail
scientifique, les chercheurs utilisant la publication comme moyen de
diffusion des connaissances.

\subsection{\dots En informatique}

Aux débuts de l'informatique, le développement du logiciel n'était
qu'une activité annexe pour les constructeurs d'ordinateurs, et les
logiciels étaient diffusés avec leur code source. Le code source est
la version intelligible et compréhensible par un humain d'un programme
informatique. Il est écrit dans un langage dit de {\em programmation}
qui décrit à l'aide de mots et de formules le fonctionnement précis
d'un logiciel. Ce code source n'est pas directement utilisable par
l'ordinateur, il est donc traduit en code {\em machine}, ou code {\em
exécutable}.

Grâce à ce code source, les utilisateurs des ordinateurs,
principalement des chercheurs à l'époque, pouvaient améliorer et
corriger les logiciels en toute liberté et s'échanger les
modifications pour permettre à tous d'en bénéficier.

Au début des années 80, cette règle tacite de partage des
connaissances a changé. La multiplication des ordinateurs et
l'apparition des ordinateurs personnels ont permis la création de
nouvelles entreprises : les éditeurs de logiciels. Ces entreprises
avaient pour seule activité la création et la vente de logiciels, ces
derniers étant vendus sans leur code source. Ces logiciels dits {\em
propriétaires} sont à l'heure actuelle vendus avec la majorité des
ordinateurs personnels.

Si l'on fait une analogie avec un plat cuisiné, on peut dire qu'un
logiciel propriétaire équivaut à un plat dont on ne peut connaître la
recette. Il serait impossible d'essayer d'améliorer celle-ci ou d'en
deviner le contenu, et il serait également interdit de partager ce
plat cuisiné avec ses amis.

Malgré cette privatisation progressive des connaissances, des
chercheurs continuaient de rendre accessible et modifiable le code
source de leurs programmes, par exemple à l'Université de Berkeley aux
États-Unis. Mais le concept de {\em Logiciel Libre} n'existait pas
encore, et c'est le projet {\em GNU} qui va réellement le lancer.

\subsection{Le projet GNU}

Richard M. Stallman\footnote{souvent appelé par ses initiales : RMS},
un chercheur en informatique au MIT\footnote{Institut de Technologie
du Massachusetts, États-Unis}, considère que le logiciel propriétaire
divise les utilisateurs : livré sans son code source, il empêche un
programmeur de le modifier ou de l'améliorer, et il interdit de le
donner à son voisin, sa copie étant le plus souvent illégale.

Afin de faire perdurer l'esprit de partage des connaissances, il
décide de quitter son laboratoire en 1984, et de se consacrer à
l'écriture d'un système informatique complet et libre, appelé \og GNU
\fg~\cite{gnu}, pour \textsl{GNU's Not \unix}\footnote{\textsl{GNU
N'est pas \unix} en français}. \unix est un type de système informatique
propriétaire utilisé à l'époque sur de nombreux ordinateurs.
L'objectif de GNU est d'être similaire à \unix, pour être rapidement
utilisable, mais ce n'est pas \unix, d'où son nom.

Pour promouvoir et soutenir le projet GNU, il crée en 1985 la Free
Software Foundation\footnote{Fondation pour le Logiciel Libre en
français, aussi connu sous l'acronyme FSF}. Les premiers travaux de
cette fondation furent de définir le concept de Logiciel Libre et de
rédiger un document définissant les conditions d'utilisation d'un
Logiciel Libre : la licence publique générale GNU, ou GPL~\cite{gpl}.
Le projet GNU a ainsi posé les fondations éthiques et juridiques du
Logiciel Libre.

%% TODO :  paragraphe à déplacer ailleurs
%% \section{Le Libre : intérêt collectif et bien commun}

%% Pour construire les biens publics, souvent coûteux et indispensables
%% comme l'eau courante, l'électricité, le tout-à-l'égout ou la voirie,
%% les gens mettent en commun leurs ressources et leurs compétences pour
%% satisfaire à ces besoins. Ce point de vue est totalement respecté par
%% l'éthique du Libre : chacun participe à sa manière pour construire des
%% \oe{}uvres qui pourront être reprises et utilisées par l'humanité toute
%% entière, sans aucune limitation ni dans leur utilisation, ni dans leur
%% adaptation. Le libre est la solution à choisir naturellement pour une
%% \oe{}uvre d'intérêt public et de bien commun. Son application au domaine
%% du logiciel a été simple car un logiciel, contrairement à une voiture
%% par exemple, est un bien immatériel qui peut être reproduit à l'infini
%% à un coût quasi nul.
